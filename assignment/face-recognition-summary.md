
**Face  Recognition**

Face recognition is a computer vision task of identifying and verifying a person based on a photograph of their face.

It is the general task of identifying and verifying people from photographs of their face.

**FaceNet**  is a face recognition system developed in 2015 by researchers at Google that achieved then state-of-the-art results on a range of face recognition benchmark datasets.

The FaceNet system can be used to extract high-quality features from faces, called face embeddings, that can then be used to train a face identification system.

Below figure is the workflow of general face recognition system.

![](https://arsfutura-production.s3.us-east-1.amazonaws.com/magazine/2019/10/face_recognition/face-recognition-pipeline.png)

**1.** Face detection — Detecting one or more faces in an image.

**2.** Feature extraction — Extracting the most important features from an image of the face.

**3.** Face classification — Classifying the face based on extracted features.

Below is the working process of Facenet model.

![](https://arsfutura-production.s3.us-east-1.amazonaws.com/magazine/2019/10/face_recognition/facenet-brki.png)

**1.** FaceNet takes an image of the person’s face as input and outputs a vector of 128 numbers which represent the most important features of a face.

**2.** The vector used here is embedding vector which helps in interpreting  vectors as points in the Cartesian coordinate system. That means we can plot an image of a face in the coordinate system using its embeddings. So similar faces (same kind) are grouped in cluster as shown below.

![](https://arsfutura-production.s3.us-east-1.amazonaws.com/magazine/2019/10/face_recognition/facenet-brki-ana.png)

**3.** By doing so , we can do further analysis and accordingly recognize face from the input image. 