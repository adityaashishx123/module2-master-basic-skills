**Face  Detection**

Face detection is a problem in computer vision of locating and localizing one or more faces in a photograph.

Locating a face in a photograph refers to finding the coordinate of the face in the image, whereas localization refers to demarcating the extent of the face, often via a bounding box around the face.

It is a non-trivial computer vision problem for identifying and localizing faces in images.

State-of-the-art face detection can be achieved using a Multi-task Cascade CNN via the MTCNN library.

MTCNN is able to detect face and in addition , also able to recognize other facial features such as eyes and mouth, called landmark detection.

MTCNN model is comprised of three sub-models namely P-Net, R-Net and O-Net which is shown in the link.![P-net](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/24.png)  ![R-net](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/25.png)  ![O-net](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/26.png)


Below figure is the output of this State-of-the-art face detection model.

![output](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/1.png)

**Workflow of Multi-Task Cascaded Convolutional Neural Network :-**

**1.** First the image is rescaled to a range of different sizes (called an image pyramid). ![First_Step](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/20.png)

**2.** Then the rescaled image is passed to a Proposal network (P-Net) proposes candidate facial regions. ![Second_Step](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/21.png)

**3.** Then the candidate facial regions are filtered by Refine Network (R-Net) model.![Third_Step](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/22.png)

**4.** And in the last stage , Output Network (O-Net) model proposes facial landmarks.![Fourth_Step](https://gitlab.com/adityaashishx123/module2-master-basic-skills/-/blob/master/images/23.png)